class User < ActiveRecord::Base
  has_secure_password

  validates :first_name, :last_name, :email, presence: true
  #this validation occurs both at creation and updating
  #validates :password, length: {greater_than: 8}, on: :create
end
